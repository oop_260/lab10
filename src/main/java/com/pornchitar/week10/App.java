package com.pornchitar.week10;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        Rectangle rec1 = new Rectangle(5, 3);
        System.out.println(rec1.toString());
        System.out.printf("%s area : %.1f \n",rec1.getName(),rec1.calArea());
        System.out.printf("%s perimeter : %.1f \n",rec1.getName(),rec1.calPerimeter());

        Rectangle rec2 = new Rectangle(2, 2);
        System.out.println(rec2.toString());
        System.out.printf("%s area : %.1f \n",rec2.getName(),rec2.calArea());
        System.out.printf("%s perimeter : %.1f \n",rec2.getName(),rec2.calPerimeter());

        Circle circle1 = new Circle(2);
        System.out.println(circle1);
        System.out.printf("%s area : %.3f \n",circle1.getName(),circle1.calArea());
        System.out.printf("%s perimeter : %.3f \n",circle1.getName(),circle1.calPerimeter());

        Circle circle2 = new Circle(3);
        System.out.println(circle2);
        System.out.printf("%s area : %.3f \n",circle2.getName(),circle2.calArea());
        System.out.printf("%s perimeter : %.3f \n",circle2.getName(),circle2.calPerimeter());

        Triangle triangle = new Triangle(2,2,2);
        System.out.println(triangle.toString());
        System.out.printf("%s area : %.3f \n",triangle.getName(),triangle.calArea());
        System.out.printf("%s perimeter : %.3f \n",triangle.getName(),triangle.calPerimeter());

    }
}
